/**
 * Created by mx on 08.04.16.
 */

var toDoList = angular.module('toDoList', []);

toDoList.controller('ToDoListController', function ($scope) {
    var tasks =  localStorage.getItem("toDoes");

    tasks = JSON.parse(tasks);
    if(tasks == null)  {
        tasks = [];
        // Todo: show "no data found"
    }
    $scope.orderProp = 'id';
    $scope.tasks = tasks;
});
