;(function(scope) {
    /**
     * Object constructor
     * Custom logics
     *
     * @param options
     * @constructor
     */
    var TODOList = function (options) {
        var self = this;
        // DOM elements
        self.addForm = document.getElementById('add-new-work-form');
        self.editForm = document.getElementById('edit-work-form');
        // data object
        self.todoes = localStorage.getItem("toDoes");
        self.todoes = JSON.parse(self.todoes);

        if(self.todoes == null)  {
            self.todoes = [];
        }
    };

    /**
     * Init method to start with
     */
    TODOList.prototype.init = function () {
        var self = this;
        self.onAddNewButton();
        self.onCloseTaskButton();
        self.onDeleteTaskButton();
        self.onEditTaskButton();
    };


    /**
     * Handle click on the add new task button
     * @param item
     */
    TODOList.prototype.onAddNewButton = function (item) {
        var self = this,
            addNewButton = document.getElementById('add-new-button');
        addNewButton.onclick = function() {
            self.showCustomPopup();
        };

    };


    /**
     * Makes an overvay visible and show a new/edit form
     *
     * @param item object
     */
    TODOList.prototype.showCustomPopup = function (item) {
        var self = this,
            overlay = document.getElementById('overlay'),
            form = document.getElementById('add-new-work-form'),
            formLength = form.elements.length,
            id = item ? item.id : false,
            f;
        overlay.style.visibility = (overlay.style.visibility == "visible") ? "hidden" : "visible";

        if (item) {

            for (f = 0; f < formLength; f++) {
                // get field
                form.elements[f].value = item[form.elements[f].name];
            }
        }

        self.onSubmitButton(form, id);
    };


    /**
     * Run actions on the edit/add task form submit
     *
     * @param form
     * @param id
     */
    TODOList.prototype.onSubmitButton = function (form, id) {
        var self = this,
            submitButton = document.getElementById('push-new-object');
        submitButton.onclick = function () {
            if ( self.validateForm(form) ) {
                overlay.style.visibility = (overlay.style.visibility == "visible") ? "hidden" : "visible";
                self.saveToStorage(form,id);
            }
        }
    };


    /**
     * Save item to storage
     *
     * @param form
     * @param id - int if object needs to be edited
     * @returns {boolean}
     */
    TODOList.prototype.saveToStorage = function (form, id) {
        var self = this,
            currentId = localStorage.getItem("currentId"),
            formLength = form.elements.length,
            formValues = {},
            f, field, newTodo;
        if (currentId == null) {
            currentId = 0;
        }
        
        currentId = currentId ? currentId : 0;

        for (f = 0; f < formLength; f++) {
            field = form.elements[f];
            formValues['id'] = id ? id : currentId;
            formValues['status'] = 0;
            formValues[field.name] = field.value;
        }

        self.todoes.push(formValues);
        localStorage.setItem("toDoes", JSON.stringify(self.todoes));
        if (!id) {
            localStorage.setItem("currentId", ++currentId);
        }
        location.reload();
        return true;
    };


    /**
     * validates form on empty strings
     * @param form
     * @returns {boolean}
     */
    // todo: create enhanced validation
    TODOList.prototype.validateForm = function (form) {
        var formLength = form.elements.length,
            validity = true,
            f, value;
        for (f = 0; f < formLength; f++) {
            // get field
            value = form.elements[f].value;
            if (value == null || value == "") {
                console.error('validation failed');
                validity = false;
            }
        }
        return validity;
    };


    /**
     * on close task button, save status property
     */
    TODOList.prototype.onCloseTaskButton = function () {
        var self = this,
            CLOSE_TASK_CLASS = 'close-task';
        document.querySelector('body').addEventListener('click', function(event) {
            var target = event.target;
            if (target.className === CLOSE_TASK_CLASS) {
                var taskId = target.getAttribute('data-id');
                for (var i=0; i < self.todoes.length; i++) {
                    if (self.todoes[i].id == taskId) {
                        self.todoes[i].status = 1;
                        localStorage.setItem("toDoes", JSON.stringify(self.todoes));
                        location.reload()
                    }
                }
            }
        });
    };


    /**
     * delete task
     */
    TODOList.prototype.onDeleteTaskButton = function () {
        var self = this,
            DELETE_TASK_CLASS = 'delete-task';
        document.querySelector('body').addEventListener('click', function(event) {
            var target = event.target;
            if (target.className === DELETE_TASK_CLASS) {
                var taskId = target.getAttribute('data-id');
                for (var i=0; i < self.todoes.length; i++) {
                    if (self.todoes[i].id == taskId) {
                        self.todoes.splice(i, 1);
                        localStorage.setItem("toDoes", JSON.stringify(self.todoes));
                        location.reload()
                    }
                }
            }
        });
    };


    /**
     * edit task button handler
     */
    TODOList.prototype.onEditTaskButton = function () {
        var self = this,
            EDIT_TASK_CLASS = 'edit-task';
        document.querySelector('body').addEventListener('click', function(event) {
            var target = event.target;
            if (target.className === EDIT_TASK_CLASS) {
                var taskId = target.getAttribute('data-id');
                for (var i=0; i < self.todoes.length; i++) {
                    if (self.todoes[i].id == taskId) {
                        self.showCustomPopup(self.todoes[i]);
                        self.todoes.splice(i, 1);
                    }
                }
            }
        });
    };


    // instance creation
    var instance = new TODOList();
    instance.init();

})(window);




